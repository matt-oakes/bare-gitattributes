require 'rugged'

repoUrl = '../diff-example-bare'
repo = Rugged::Repository.new(repoUrl)

# Look up the SHA ID for HEAD
ref = repo.head

# Figure out what blob OID is there for .gitattributes
tree = ref.target.tree
puts tree
gitattributes = tree.find { |entry| entry if entry[:type] == :blob and entry[:name] == '.gitattributes' }
puts gitattributes

# Retrieve the contents of the blob
blob = repo.lookup(gitattributes[:oid])
puts blob.content

# Copy this file into the bare repo info/attributes.
filename = repoUrl + '/info/attributes'
target = open(filename, 'w')
target.write(blob.content)
target.close

# Get the diff
diff = repo.diff('da222d2f71ece0fe41c53dfccdb7a69514182364', "068f578045b4983ad5b8bdde1f563fa620824860", {})
puts diff.patch
